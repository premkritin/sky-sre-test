module "vm" {
    source = "./modules/vm"
    cpu   = var.cpu
    memory = var.memory
    name   = var.name
    vmtemp = var.vmtemp
    instances = var.instances

}