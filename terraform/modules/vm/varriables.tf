variable "cpu" {}
variable "memory" {}
variable "name" {}
variable "vmtemp" {}
variable "instances" {}
variable "vmnameformat" {
    default     = "%02d"
}

variable "staticvmname" {
  default     = null
}